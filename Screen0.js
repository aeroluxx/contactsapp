import React, { useState, useEffect } from 'react'
import { StyleSheet, View, ActivityIndicator, Alert, Modal } from 'react-native'
import { Header } from './src/components/Header'
import { TapBar } from './src/components/TapBar'
import { Screen1 } from './src/screens/Screen1'
import { Screen2 } from './src/screens/Screen2'
import { ModalScreen } from './src/screens/ModalScreen'
import { asyncHandler } from './src/helpers'

const screenTitles = ['Contacts', 'Log In']

const url = 'https://jsonplaceholder.typicode.com/users'

const App = () => {
  const [activeScreen, setActiveScreen] = useState(1)

  const [isModalVisible, setIsModalVisible] = useState(false)

  const [isLoading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const [refresh, setRefresh] = useState(false)

  const [pickerValue, pickerValueChange] = useState(3)

  useEffect(() => {
    asyncHandler(url, setData, setLoading, alertHandler)
  }, [refresh])

  const alertHandler = (error) =>
    Alert.alert(
      `${error}`,
      'Repeat the request?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel'),
          style: 'cancel'
        },
        { text: 'OK', onPress: () => setRefresh(!refresh) }
      ],
      { cancelable: false }
    )

  const pickerHandler = (item) => {
    pickerValueChange(item)
    setTimeout(() => setIsModalVisible(!isModalVisible), 1000)
  }

  if (isLoading) {
    return <ActivityIndicator style={styles.indicatorStyle} size="large" color="black" />
  }

  return (
    <View style={styles.root}>
      <ModalScreen isModalVisible={isModalVisible} pickerHandler={pickerHandler} pickerValue={pickerValue} />
      <Header
        titlesArray={screenTitles}
        activeScreen={activeScreen}
        setActiveScreen={setActiveScreen}
        pickerValue={pickerValue}
      />
      {activeScreen === 1 && <Screen1 data={data} setIsVisible={() => setIsModalVisible(!isModalVisible)} />}
      {activeScreen === 2 && <Screen2 />}
      <TapBar setActiveScreen={setActiveScreen} />
    </View>
  )
}

const styles = StyleSheet.create({
  scrollStyle: { flex: 1 },
  scrollContainer: {
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  root: {
    paddingHorizontal: 25,
    flex: 1
  },
  inputStyle: {
    width: '100%',
    height: 55,
    margin: 12,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    paddingLeft: 30,
    borderRadius: 20,
    fontSize: 18
  },
  indicatorStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalStyle: {
    flex: 1
  },
  pickerBox: {
    flex: 1,
    backgroundColor: 'white',
    opacity: 0.85,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default App
