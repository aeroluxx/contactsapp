import AsyncStorage from '@react-native-async-storage/async-storage'
import { Alert, Platform } from 'react-native'
import RNFetchBlob from 'rn-fetch-blob'

const isIos = Platform.OS === 'ios'

const alertHandler = (error, refreshHandler) =>
  Alert.alert(
    `${error}`,
    'Repeat the request?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel'),
        style: 'cancel'
      },
      { text: 'OK', onPress: () => refreshHandler() }
    ],
    { cancelable: false }
  )

// export const fetchHandler = (url, setData, setLoading, refreshHandler) => {
//   fetch(url)
//     .then((response) => response.json())
//     .then((responseJson) => {
//       setData(responseJson)
//     })
//     .catch((error) => {
//       setLoading(false)
//       alertHandler(error, refreshHandler)
//     })
//     .finally(() => {
//       setLoading(false)
//     })
// }

export const asyncHandler = async (url, page, setData, setLoading, refreshHandler) => {
  try {
    const response = await fetch(url + page)
    const users = await response.json()
    setData(users.results)
    setLoading(false)
  } catch (error) {
    setLoading(false)
    alertHandler(error, refreshHandler)
  }
}

export const getFromStorage = async (callback) => {
  try {
    const value = await AsyncStorage.getItem('AUTORIZED')
    callback(Boolean(value))
  } catch (error) {
    console.log(error)
  }
}

const randomNameGen = () => {
  const timestamp = new Date().toISOString().replace(/[-:.]/g, '')
  // console.log('date:::', new Date().toISOString().replace(/[-:.]/g, ''))
  const random = `${Math.random()}`.substring(2, 8)
  return timestamp + random
}

export const downloadFile = async (url) => {
  const {
    dirs: { DocumentDir, DownloadDir }
  } = RNFetchBlob.fs

  const dirToSave = isIos ? DocumentDir : DownloadDir
  const fileName = `${randomNameGen()}.png`

  const config = {
    fileCache: true,
    useDownloadManager: true,
    notification: true,
    mediaScannable: true,
    title: fileName,
    path: `${dirToSave}/${fileName}`
  }

  const configOptions = isIos
    ? {
        fileCache: config.fileCache,
        title: config.title,
        path: config.path,
        appendExt: 'png'
      }
    : config

  RNFetchBlob.config(configOptions)
    .fetch('GET', `${url}`, {})
    .then((res) => {
      if (isIos) {
        RNFetchBlob.fs.writeFile(config.path, res.data, 'base64').then(() => {
          RNFetchBlob.ios.previewDocument(config.path)
        })
      }
      if (!isIos) {
        try {
          RNFetchBlob.android.addCompleteDownload({
            title: config.title,
            mime: 'image/png',
            path: config.path,
            showNotification: true
          })
        } catch (error) {
          console.log(error)
        }
      }
    })
    .catch((e) => {
      console.log(e.message)
    })
}
