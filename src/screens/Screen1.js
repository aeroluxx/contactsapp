/* eslint-disable no-console */
import React, { useState, useEffect } from 'react'
import { StyleSheet, View, ActivityIndicator, Alert } from 'react-native'
import { Header } from '../components/Header'
import { UsersList } from '../components/UsersList'
import { ModalScreen } from './ModalScreen'
import { ModalContact } from './ModalContact'
import { asyncHandler } from '../helpers'

const url = 'https://randomuser.me/api/?results=5&_page='

const Screen1 = () => {
  const [isLoading, setLoading] = useState(true)
  const [data, setData] = useState([])
  const [refresh, setRefresh] = useState(false)

  const [page, setPage] = useState(1)
  const [updatedData, setUpdatedData] = useState(data)

  const [isModalVisible, setIsModalVisible] = useState(false)
  const [pickerValue, pickerValueChange] = useState('Country')

  const [isModalContactVisible, setIsModalContactVisible] = useState(false)
  const [currentUser, setCurrentUser] = useState({})
  const [userIndex, setUserIndex] = useState(null)

  const refreshHandler = () => {
    setRefresh(!refresh)
  }

  useEffect(() => {
    console.log('page:::', page)
    asyncHandler(url, page, setData, setLoading, refreshHandler)
  }, [page, refresh])

  // console.log('page:::', page)
  // console.log('updatedData:::', updatedData)
  // console.log('data:::', data)

  useEffect(() => {
    if (page === 1) {
      setUpdatedData(data)
    } else {
      setUpdatedData(updatedData.concat(data))
    }
  }, [data])

  const setPageHandle = () => {
    page <= 4 ? setPage(page + 1) : null
  }

  useEffect(() => {
    const user = data[userIndex]
    const userData = {
      userName: `${user?.name?.first} ${user?.name?.last}`,
      userPhone: user?.phone
    }
    setCurrentUser(userData)
  }, [userIndex])

  const pickerHandler = (item, array) => {
    pickerValueChange(item)
    const filteredData = array.filter((element) => element.location.country === pickerValue)
    setUpdatedData(filteredData)
    setTimeout(() => setIsModalVisible(!isModalVisible), 500)
  }

  if (isLoading) {
    return <ActivityIndicator style={styles.indicatorStyle} size="large" color="black" />
  }

  return (
    <>
      <ModalContact
        isModalContactVisible={isModalContactVisible}
        setIsModalContactVisible={() => setIsModalContactVisible(!isModalContactVisible)}
        currentUser={currentUser}
        userIndex={userIndex}
        setUserIndex={setUserIndex}
      />
      <View style={styles.root}>
        <ModalScreen
          isModalVisible={isModalVisible}
          pickerHandler={pickerHandler}
          pickerValue={pickerValue}
          updatedData={updatedData}
        />

        <Header title="Contacts" />
        {/* <Header title="Contacts" pickerValue={pickerValue} setIsVisible={() => setIsModalVisible(!isModalVisible)} /> */}
        <UsersList
          data={updatedData}
          extraData={page}
          setIsVisible={() => setIsModalVisible(!isModalVisible)}
          setUserIndex={setUserIndex}
          setIsModalContactVisible={() => setIsModalContactVisible(!isModalContactVisible)}
          setPageHandle={setPageHandle}
        />
      </View>
    </>
  )
}

export default Screen1

const styles = StyleSheet.create({
  scrollStyle: { flex: 1 },
  scrollContainer: {
    paddingTop: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  root: {
    paddingHorizontal: 25,
    flex: 1
  },
  inputStyle: {
    width: '100%',
    height: 55,
    margin: 12,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    paddingLeft: 30,
    borderRadius: 20,
    fontSize: 18
  },
  indicatorStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalStyle: {
    flex: 1
  },
  pickerBox: {
    flex: 1,
    backgroundColor: 'white',
    opacity: 0.85,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
