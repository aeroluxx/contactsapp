import React, { useEffect, useState } from 'react'
import { StyleSheet, FlatList, View, Alert, ActivityIndicator, Modal } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { Header } from '../components/Header'
import CardPhoto from '../components/CardPhoto'
import { ModalPhoto } from './ModalPhoto'

const pickerArray = [
  {
    title: 'Album One',
    value: 1
  },
  {
    title: 'Album Two',
    value: 2
  },
  {
    title: 'Album Three',
    value: 3
  },
  {
    title: 'Album Four',
    value: 4
  }
]

const Screen3 = () => {
  const [isLoading, setLoading] = useState(true)
  const [page, setPage] = useState(1)
  const [data, setData] = useState([])
  const [pickerValue, pickerValueChange] = useState(1)
  const [modalVisible, setModalVisible] = useState(false)
  const [first, setFirst] = useState(true)
  const [refresh, setRefresh] = useState(false)

  const [currentPhoto, setCurrentPhoto] = useState({})
  const [photoIndex, setPhotoIndex] = useState(null)

  const [isModalContactVisible, setIsModalContactVisible] = useState(false)

  const handleLoadMore = () => {
    setPage(page + 1)
  }

  const alertHandler = (error) =>
    Alert.alert(
      `${error}`,
      'Repeat the request?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel'),
          style: 'cancel'
        },
        { text: 'OK', onPress: () => setRefresh(!refresh) }
      ],
      { cancelable: false }
    )

  const fetchHandler = () => {
    const url = `https://jsonplaceholder.typicode.com/photos?_limit=10&_page=${String(page)}`
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        setData(first ? responseJson : data.concat(responseJson))
      })
      .catch((error) => {
        console.error(error)
        alertHandler()
      })
      .finally(() => {
        setLoading(false)
        setFirst(false)
      })
  }

  useEffect(() => {
    fetchHandler()
  }, [page, setPage])

  const pickerHandler = (item) => {
    setPage(item * 5 - 4)
    setFirst(true)
    pickerValueChange(item)
    const filteredData = data.filter((x) => x.albumId === pickerValue)
    setData(filteredData)
    setTimeout(() => setModalVisible(!modalVisible), 1000)
  }

  useEffect(() => {
    const photo = data.find(({ id }) => id === photoIndex)
    const photoData = {
      url: photo?.url,
      title: photo?.title
    }
    setCurrentPhoto(photoData)
  }, [photoIndex])

  const renderItem = ({ item }) => (
    <CardPhoto
      item={item}
      setIsModalContactVisible={() => setIsModalContactVisible(!isModalContactVisible)}
      setPhotoIndex={() => setPhotoIndex(item.id)}
    />
  )

  return (
    <>
      <ModalPhoto
        isModalContactVisible={isModalContactVisible}
        setIsModalContactVisible={() => setIsModalContactVisible(!isModalContactVisible)}
        currentPhoto={currentPhoto}
        photoIndex={photoIndex}
        setPhotoIndex={setPhotoIndex}
      />
      <View style={styles.root}>
        <Header title="Gallery" pickerValue={pickerValue} setIsVisible={() => setModalVisible(true)} />
        {isLoading ? (
          <ActivityIndicator style={styles.indicatorStyle} size="large" color="white" />
        ) : (
          <FlatList
            refreshing={isLoading}
            onRefresh={() => fetchHandler()}
            contentContainerStyle={styles.flatListStyle}
            data={data}
            extraData={pickerValue}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={handleLoadMore}
            onEndReachedThreshold={0}
          />
        )}
        <Modal animationType="slide" transparent={false} visible={modalVisible} style={styles.modalStyle}>
          <View style={styles.pickerBox}>
            <Picker
              selectedValue={pickerValue}
              style={{ height: 200, width: '100%', paddingHorizontal: 24 }}
              onValueChange={(itemValue, itemIndex) => pickerHandler(itemValue)}
            >
              {pickerArray.map((item) => (
                <Picker.Item label={item.title} value={item.value} key={item.value.toString} />
              ))}
            </Picker>
          </View>
        </Modal>
      </View>
    </>
  )
}

export default Screen3

const styles = StyleSheet.create({
  root: {
    paddingHorizontal: 25,
    flex: 1,
    backgroundColor: 'black'
  },
  indicatorStyle: { flex: 1 },
  pickerBox: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  }
})
