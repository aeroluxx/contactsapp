import React from 'react'
import { StyleSheet, View, Text, Alert, Image } from 'react-native'
import GestureRecognizer from 'react-native-swipe-gestures'
import { Header } from '../components/Header'
import { downloadFile } from '../helpers'
import { ButtonSubmit } from '../components/ButtonSubmit'

const config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
}

export const ModalPhoto = ({
  isModalContactVisible,
  setIsModalContactVisible,
  currentPhoto,
  setPhotoIndex,
  photoIndex
}) => {
  const swipeLeft = () => {
    setPhotoIndex(photoIndex + 1)
  }
  const swipeRight = () => {
    photoIndex - 1 < 0 ? Alert.alert("It's a Beginning") : setPhotoIndex(photoIndex - 1)
  }
  return (
    <GestureRecognizer
      onSwipeLeft={swipeLeft}
      onSwipeRight={swipeRight}
      config={config}
      style={[
        styles.modalStyle,
        {
          position: 'absolute',
          zIndex: isModalContactVisible ? 5 : -5,
          width: isModalContactVisible ? '100%' : 0,
          height: isModalContactVisible ? '100%' : 0,
          opacity: isModalContactVisible ? 1 : 0
        }
      ]}
    >
      <View style={styles.modalBox}>
        <Header setIsModalContactVisible={setIsModalContactVisible} isHaveBorder />
        <View style={styles.textBox}>
          <Image source={{ uri: currentPhoto?.url }} style={{ width: '100%', height: '50%' }} />
          <Text style={styles.userPhoneText}>{currentPhoto?.title}</Text>
        </View>
        <ButtonSubmit title="Download" onPress={() => downloadFile(currentPhoto?.url)} />
      </View>
    </GestureRecognizer>
  )
}

const styles = StyleSheet.create({
  modalStyle: {},
  modalBox: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 1,
    paddingHorizontal: 25
  },
  userNameText: {
    fontSize: 43,
    fontWeight: '400',
    color: 'white'
  },
  userPhoneText: {
    paddingTop: 15,
    fontSize: 21,
    fontWeight: '400',
    color: 'white'
  },
  textBox: {
    height: '70%',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
