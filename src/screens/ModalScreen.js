import React from 'react'
import { StyleSheet, View, Modal } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import { pickerArray } from '../dataPicker'

export const ModalScreen = ({ isModalVisible, pickerHandler, pickerValue, updatedData }) => {
  console.log('updatedData:::', updatedData)
  return (
    <Modal style={styles.modalStyle} animationType="fade" visible={isModalVisible} transparent>
      <View style={styles.pickerBox}>
        <Picker
          selectedValue={pickerValue}
          style={styles.pickerStyle}
          onValueChange={(itemValue) => pickerHandler(itemValue, updatedData)}
        >
          {pickerArray.map((item) => (
            <Picker.Item label={item.title} value={item.value} />
          ))}
        </Picker>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalStyle: {
    flex: 1
  },
  pickerBox: {
    flex: 1,
    backgroundColor: 'white',
    opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pickerStyle: {
    height: 250,
    width: '90%',
    paddingHorizontal: 24,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 25
  }
})
