import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

import Screen1 from '../screens/Screen1'
import Screen2 from '../screens/Screen2'
import Screen3 from '../screens/Screen3'

import { TapBar } from '../components/TapBar'

const RootStack = createStackNavigator()
const SecureStack = createStackNavigator()
const Tabs = createBottomTabNavigator()

const SecureScreens = () => (
  <SecureStack.Navigator initialRouteName="Screen 2" headerMode="none">
    <SecureStack.Screen name="Screen 2" component={Screen2} />
    <SecureStack.Screen name="Screen 3" component={Screen3} />
  </SecureStack.Navigator>
)

const TabScreens = () => (
  <Tabs.Navigator
    headerMode="none"
    initialRouteName="Screen 1"
    tabBar={({ navigation, state, descriptors }) => <TapBar {...{ navigation, state, descriptors }} />}
  >
    <Tabs.Screen name="Screen 1" component={Screen1} />
    <Tabs.Screen name="Screen 2" component={SecureScreens} />
  </Tabs.Navigator>
)

const Navigation = () => (
  <NavigationContainer>
    <RootStack.Navigator initialRouteName="TabScreens" headerMode="none">
      <RootStack.Screen name="TabScreens" component={TabScreens} />
    </RootStack.Navigator>
  </NavigationContainer>
)

export default Navigation
