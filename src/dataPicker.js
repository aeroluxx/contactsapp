export const pickerArray = [
  {
    title: 'Canada',
    value: 'Canada'
  },
  {
    title: 'France',
    value: 'France'
  },
  {
    title: 'Netherlands',
    value: 'Netherlands'
  },
  {
    title: 'Germany',
    value: 'Germany'
  }
]
