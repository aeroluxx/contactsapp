import React, { useState } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { ButtonCircle } from './ButtonCircle'

import { getFromStorage } from '../helpers'

export const Header = ({ title, pickerValue, setIsModalContactVisible, setIsVisible, isHaveBorder }) => {
  const [isAutorized, setAutorized] = useState(false)
  const navigation = useNavigation()
  const isGallery = title === 'Gallery'

  const navigateHandle = () => {
    if (title === 'Gallery' && isAutorized) {
      navigation.navigate('TabScreens', { screen: 'Screen 1' })
    }
    setIsModalContactVisible ? setIsModalContactVisible() : navigation.goBack()
  }

  getFromStorage(setAutorized)

  return (
    <View style={styles.root}>
      {title !== 'Contacts' && <ButtonCircle onPress={navigateHandle} isHaveBorder={isGallery || isHaveBorder} />}
      <Text style={[styles.headerText, { color: isGallery ? 'white' : '#0A0A0A' }]}>{title}</Text>
      <TouchableOpacity
        onPress={setIsVisible}
        style={[styles.buttonStyle, { backgroundColor: isGallery ? '#00ADD3' : 'transparent' }]}
      >
        <Text
          style={[styles.headerText, { color: isGallery ? 'white' : '#0A0A0A', fontWeight: isGallery ? '200' : '100' }]}
        >
          {pickerValue}
        </Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  root: {
    paddingTop: 50,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  headerText: {
    fontSize: 36,
    fontWeight: '100'
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#0A0A0A',
    borderRadius: 20,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconStyle: {
    height: 20,
    width: 15
  },
  buttonStyle: {
    backgroundColor: '#00ADD3',
    width: 40,
    height: 40,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
