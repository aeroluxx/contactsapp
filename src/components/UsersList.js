import React from 'react'
import { FlatList } from 'react-native'
import { UserCard } from './UserCard'

export const UsersList = ({ data, setIsVisible, setUserIndex, setIsModalContactVisible, setPageHandle }) => {
  const renderItem = ({ item, index }) => (
    <UserCard
      userName={`${item?.name?.first} ${item?.name?.last}`}
      userPhone={item?.phone}
      userPic={item?.userPic}
      userAddress={`${item?.location?.country}, ${item?.location?.city}`}
      userMail={item?.email}
      setIsVisible={setIsVisible}
      setIsModalContactVisible={() => setIsModalContactVisible()}
      setCurrentUserIndex={() => setUserIndex(index)}
    />
  )

  return (
    <FlatList
      data={data}
      extraData={data}
      renderItem={renderItem}
      keyExtractor={(item, index) => index}
      onEndReached={() => setPageHandle()}
      onEndReachedThreshold={1}
    />
  )
}
