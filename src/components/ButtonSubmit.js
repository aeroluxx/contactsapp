import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'

export const ButtonSubmit = ({ onPress, title }) => (
  <TouchableOpacity style={styles.buttonStyle} onPress={onPress}>
    <Text style={styles.buttonTextStyle}>{title}</Text>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  buttonStyle: {
    width: '75%',
    borderRadius: 25,
    paddingHorizontal: 15,
    height: 50,
    marginVertical: 5,
    backgroundColor: '#00ADD3',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonTextStyle: {
    fontSize: 16,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center'
  }
})
