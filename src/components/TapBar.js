import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useAppState } from '@react-native-community/hooks'

export const TapBar = ({ navigation, state }) => {
  const [isAutorized, setAutorized] = useState(false)
  const currentAppState = useAppState()

  const clearStorage = async () => {
    try {
      await AsyncStorage.removeItem('AUTORIZED')
    } catch (error) {
      console.log(error)
    }
  }

  const getFromStorage = async () => {
    try {
      const value = await AsyncStorage.getItem('AUTORIZED')
      setAutorized(Boolean(value))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    if (currentAppState === ('background' || 'inactive')) {
      clearStorage()
    }
  }, [currentAppState])

  useEffect(() => {
    getFromStorage()
  })

  const handleNavigate = (item) => {
    if (isAutorized && item.name === 'Screen 2') {
      navigation.navigate('Screen 2', { screen: 'Screen 3' })
    }
    if (!isAutorized) {
      navigation.navigate(item.name)
    }
  }

  if (state?.index === 1) return null
  return (
    <View style={styles.root}>
      {state?.routes.map((item) => (
        <TouchableOpacity style={styles.buttonStyle} onPress={() => handleNavigate(item)}>
          <Text style={styles.headerText}>{item.name.toUpperCase()}</Text>
        </TouchableOpacity>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  root: {
    marginBottom: 10,
    backgroundColor: '#0A0A0A',
    borderRadius: 20,
    width: '70%',
    height: 50,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 40,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  headerText: {
    fontSize: 15,
    fontWeight: '700',
    color: '#FFFFFF',
    textAlign: 'center'
  },
  buttonStyle: {
    flex: 1
  }
})
